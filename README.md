## Mini CRM

This is a small CRM system implemented in Git repository. It includes companies and their employees, which can be modified or deleted. It also has an admin login interface for secure access.

# Features

- Add new companies and their employees
- View and modify existing company and employee data
- Delete companies and their employees
- Secure admin login interface

# Usage
To use the CRM system, please follow these steps:

1. Open the browser and navigate to http://localhost:8000
2. Login using the admin credentials
3. Add, modify, or delete companies and employees as needed

## Contributing
If you would like to contribute to the CRM system, please follow these steps:

1. Fork the repository
2. Create a new branch for your feature or bug fix
3. Make changes and commit them to your branch
4. Push your changes to your forked repository
5. Create a pull request to merge your changes with the main repository
